import React from "react";
import { Grid } from "@material-ui/core";

//Componenets
import Price from "../Price";
import NameCoin from "../NameCoin";

interface Props {
  coin: any;
}

const HeaderDetailCoin = ({ coin }: Props) => {

  return (
    <Grid
      container
      spacing={2}
      direction="row"
      justifyContent="flex-end"
      alignItems="center"
      key={coin.id}
    >
      <Grid
        container
        spacing={2}
        item
        xs={12}
        md={6}
        lg={6}
      >
        <NameCoin name={coin.name} symbol={coin.symbol}/>
        <Price price={coin.priceUsd} state={coin.state} />
      </Grid>
    </Grid>
  );
};

export default HeaderDetailCoin;

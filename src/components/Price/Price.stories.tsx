import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import Price from '.';

export default {
  title: 'Price',
  component: Price,
  argTypes: {
    state: {
      type: 'boolean'
    },
  },
} as ComponentMeta<typeof Price>;

const Template: ComponentStory<typeof Price> = (args) => <Price {...args} />;

export const PriceUp = Template.bind({});
PriceUp.args = {
  price: 30,
};

import { makeStyles } from "@material-ui/core/styles";

export const usePriceStyle = makeStyles((theme: any) => ({
  icon:{
    width: 40,
    height: 40,
    margin: 10
  },
  colorDown:{
    color: "red",
  },
  colorUp:{
    color: "green",
  },
  textUp:{
    color: "green",
    fontWeight: "bold",
    fontSize: 15
  },
  textDown:{
    color: "red",
    fontWeight: "initial",
    fontSize: 13
  }
  }));

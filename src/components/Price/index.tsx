import React from "react";
import { Grid, ListItemText, Typography } from "@material-ui/core";
import { TrendingUp, TrendingDown } from "@material-ui/icons";

import { usePriceStyle } from "./Price.style";

interface Props {
  price: any;
  state: string;
}

const Price = ({ price, state }: Props) => {
  const classes = usePriceStyle();
  return (
    <ListItemText
      primary={
        <Grid container direction="row" alignItems="center">
          <Typography>Precio</Typography>
          {state ? (
            <TrendingDown className={classes.icon && classes.colorDown} />
          ) : (
            <TrendingUp className={classes.icon && classes.colorUp} />
          )}
        </Grid>
      }
      secondary={
        <Typography className={!state ? classes.textUp : classes.textDown}>
          {parseFloat(price).toFixed(4)}
        </Typography>
      }
    />
  );
};

export default Price;

import React from 'react'
import { ListItemAvatar, ListItemText } from "@material-ui/core";

//Style
import Icon from "react-crypto-icons";

interface Props {
    name: string
    symbol: string
}

const NameCoin = ({name, symbol}: Props) => {

    return (
        <>
        <ListItemAvatar>
            <Icon
            name={`${symbol.toLowerCase()}`}
            size={50}
            />
        </ListItemAvatar>
        <ListItemText primary={name} secondary={symbol} />
     </> );
}

export default NameCoin;
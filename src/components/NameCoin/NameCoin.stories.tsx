import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import NameCoin from '.';

export default {
  title: 'NameCoin',
  component: NameCoin,
  argTypes: {
    state: {
      type: 'boolean'
    },
  },
} as ComponentMeta<typeof NameCoin>;

const Template: ComponentStory<typeof NameCoin> = (args) => <NameCoin {...args} />;

export const PriceUp = Template.bind({});
PriceUp.args = {
  name: 'bitcoin',
  symbol: 'bitcoin',
};

import React, { useEffect, useState, useRef } from "react";
import { createChart } from "lightweight-charts";

import { optionGrapich, styleGrapich }  from './optionGrapich';

const Graphic = ({ data }: any) => {
  const chart = useRef<any>();
  const [lineSeries, setLineSeries] = useState<any>();

  useEffect(() => {
    chart.current = createChart(chart.current, optionGrapich);
    setLineSeries(() => {
      const area = chart.current.addAreaSeries(styleGrapich);
      area.setData(data);
      return area;
    });
  }, []);

  useEffect(() => {
    if (data && lineSeries) {
      lineSeries.setData(data);
    }
  }, [ data ]);

  return (
    <>
      <div ref={chart} />
    </>
  );
};

export default Graphic;

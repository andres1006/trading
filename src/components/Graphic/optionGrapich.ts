export const optionGrapich: any = {
    width: 800,
    height: 300,
    layout: {
      textColor: "#d1d4dc",
      backgroundColor: "#000000",
    },
    rightPriceScale: {
      scaleMargins: {
        top: 0.3,
        bottom: 0.25,
      },
    },
    crosshair: {
      vertLine: {
        width: 2,
        color: "rgba(224, 227, 235, 0.1)",
        style: 0,
      },
      horzLine: {
        visible: false,
        labelVisible: false,
      },
    },
    grid: {
      vertLines: {
        color: "rgba(42, 46, 57, 0)",
      },
      horzLines: {
        color: "rgba(42, 46, 57, 0)",
      },
    },
  }
export const styleGrapich: any = {
  topColor: "rgba(38, 198, 218, 0.56)",
  bottomColor: "rgba(38, 198, 218, 0.04)",
  lineColor: "rgba(38, 198, 218, 1)",
  lineWidth: 2,
  crossHairMarkerVisible: false,
}
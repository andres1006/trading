import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import Filter from '.';

export default {
  title: 'Filter',
  component: Filter,
  argTypes: {
    state: {
      type: 'boolean'
    },
  },
} as ComponentMeta<typeof Filter>;

const Template: ComponentStory<typeof Filter> = (args) => <Filter {...args} />;

export const PriceUp = Template.bind({});
PriceUp.args = {
  handleFilter: 'function',
};

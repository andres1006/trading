import React from 'react'
import {
    InputBase,
    Box
  } from "@material-ui/core";

//Styls
import SearchIcon from "@material-ui/icons/Search";
import { useFilterStyles } from "./Filter.style";
//Styls
interface Props {
    handleFilter: any

}
const Filter = ({ handleFilter }:Props) => {
    const classes = useFilterStyles();
 return (
    <Box className={classes.search}>
    <Box className={classes.searchIcon}>
      <SearchIcon />
    </Box>
    <InputBase
      placeholder="Filtar"
      classes={{
        root: classes.inputRoot,
        input: classes.inputInput,
      }}
      inputProps={{ "aria-label": "search" }}
      onChange={handleFilter}
    />
  </Box>
 )
}

export default Filter;
import React from "react";
import {
  Grid,
  ListItem,
  ListItemAvatar,
  ListItemText,
} from "@material-ui/core";

//Style
import Icon from "react-crypto-icons";

//Componenets
import Price from "../Price";
import ActionsCoin from "../ActionsCoin";
import NameCoin from "../NameCoin";

interface Props {
  coin: any;
}

const Coin = ({ coin }: Props) => {
  const veryfyFavorite = (): boolean => {
    if (localStorage.getItem("coinSuscription")) {
      var arrayCointsFavorites = JSON.parse(
        `${localStorage.getItem("coinSuscription")}`
      );
      if (
        Object.values(arrayCointsFavorites).length > 0 &&
        arrayCointsFavorites[coin.id]
      ) {
        return true;
      }
      return false;
    }
    return false;
  };

  return (
    <Grid container justifyContent="center" alignItems="center" key={coin.id}>
      <ListItem
        alignItems="center" /* style={{ backgroundColor: coin.state === "down" ? 'red': 'green' }} */
      >
        <Grid
          container
          justifyContent="center"
          alignItems="center"
          item
          xs={12}
          md={4}
          lg={4}
        >
          <NameCoin name={coin.name} symbol={coin.symbol} />
        </Grid>
        <Grid
          container
          justifyContent="center"
          alignItems="center"
          item
          xs={12}
          md={4}
          lg={4}
        >
          <Price price={coin.priceUsd} state={coin.state} />
        </Grid>
        <Grid
          container
          justifyContent="center"
          alignItems="center"
          item
          xs={12}
          md={4}
          lg={4}
        >
          <ActionsCoin coin={coin} favorite={veryfyFavorite()} />
        </Grid>
      </ListItem>
    </Grid>
  );
};

export default Coin;

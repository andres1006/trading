import { makeStyles } from "@material-ui/core/styles";

export const useActionsCoinStyle = makeStyles((theme: any) => ({
    button: {
      margin: theme.spacing(1),
    },
    iconButton: {
      margin: theme.spacing(1),
    },
  }));

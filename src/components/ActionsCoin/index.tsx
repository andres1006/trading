import React, { useState } from "react";
import { useHistory } from 'react-router-dom';
import { Button , IconButton} from "@material-ui/core";
import { StarRate, StarOutline } from "@material-ui/icons";

import { useActionsCoinStyle } from "./ActionsCoin.style";

interface Props {
  coin: any
  favorite: boolean
}

const ActionsCoin = ({ coin, favorite }: Props) => {
  const classes = useActionsCoinStyle();
  let history = useHistory();
  const [coinsFavorites, setCoinsFavorites] = useState<boolean>(favorite);

  const goToCoinDetail = () => {
    localStorage.setItem('coin', JSON.stringify(coin))
    history.push('/coin-detail');
  }

  const suscription = async () => {
    var arrayCointsFavorites = JSON.parse(`${localStorage.getItem('coinSuscription')}`) || {};
    if (localStorage.getItem('coinSuscription') &&
    Object.values(arrayCointsFavorites).length > 0
    ){
      if(coinsFavorites){
        delete arrayCointsFavorites[coin.id]
        localStorage.setItem('coinSuscription', JSON.stringify(arrayCointsFavorites))
        setCoinsFavorites(false);
        return;
      }
      arrayCointsFavorites[coin.id] = coin;
      localStorage.setItem('coinSuscription', JSON.stringify(arrayCointsFavorites))
      setCoinsFavorites(true);
      return;
    }
    arrayCointsFavorites[coin.id] = coin;
    localStorage.setItem('coinSuscription', JSON.stringify(arrayCointsFavorites))
    setCoinsFavorites(true);
  }



  return (
    <>
      <Button
        color="secondary"
        size="small"
        variant="contained"
        className={classes.button}
        onClick={goToCoinDetail}
        >
        Ver màs
      </Button>
      <IconButton
        size="small"
        aria-label="delete"
        className={classes.iconButton}
        onClick={suscription}
      >
          {
            coinsFavorites ? <StarRate color="secondary" fontSize="large" />:
            <StarOutline color="secondary" fontSize="large" />
          }
      </IconButton>

    </>
  );
};

export default ActionsCoin;

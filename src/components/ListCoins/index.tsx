import React from "react";
import {
  List,
} from "@material-ui/core";

import { useListCoinStyle } from "./ListCoins.style";
import Coin from "../Coin";

interface Props {
  listCoins: any[];
}

const ListCoins= ({ listCoins }: Props) => {
  const classes = useListCoinStyle();

  return (
    <>
      <List className={classes.heading}>
        {Object.values(listCoins).map((coin: any) => (
            <Coin coin={coin} key={coin.id}/>
        ))}
      </List>
    </>
  )
};

export default ListCoins;

import { makeStyles } from "@material-ui/core/styles";

export const useListCoinStyle = makeStyles((theme: any) => ({
    root: {
      width: "100%",
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      flexBasis: "60%",
      flexShrink: 0,
    },
    secondaryHeading: {
      fontSize: theme.typography.pxToRem(15),
      color: theme.palette.text.secondary,
      flexBasis: "20%",
      flexShrink: 0,
    },
    button: {
      margin: theme.spacing(1),
    },
  }));

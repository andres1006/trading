import React, { useContext } from "react";
import {
  Grid,
  FormControlLabel,
  Switch,
  Box,
  AppBar,
  Toolbar,
} from "@material-ui/core";
import { Link } from 'react-router-dom';

//Styles
import LogoInfosel from "../../assets/img/logo-infosel.svg";
import useStyles from "./HeaderStyle";

//Context
import AppContext from "../../context/AppContext";

const Header = () => {
  const { state, dispatch } = useContext(AppContext);
  const classes = useStyles();

  const handleDarModeEvent = (): void => {
    dispatch({ type: "HANDLE_DARK_MODE" });
  };

  return (
    <Box className={classes.root}>
      <AppBar position="static" color="primary">
        <Toolbar>
          <Grid
            container
            direction="row"
            justifyContent="space-between"
            alignItems="center"
          >
             <Link to="/" style={{ color: 'white', textDecoration: 'none'}}>
            <img src={LogoInfosel} width="100px" alt="logo"/>
            </Link>
            <FormControlLabel
              control={<Switch checked={state.darkMode} onChange={handleDarModeEvent} name="checkedA" />}
              label={state.darkMode ? "Dark Mode" : "Light Mode"}
            />
          </Grid>
        </Toolbar>
      </AppBar>
    </Box >
  );
};
export default Header;

import { useState, useCallback } from "react";

const useListCoins = (): any => {
  const [response, setResponse] = useState<any>();

  const upList = useCallback(
    (ListCoins: any, coinsUpdate: any) => {
      if (
        ListCoins &&
        Object.values(ListCoins).length > 0 &&
        Object.values(coinsUpdate).length > 0
      ) {
        Object.entries(coinsUpdate).forEach((coinUpdate: any) => {
          const keyNameCoin = coinUpdate[0];
          const ValueCoin = coinUpdate[1];
          const listCoinsUpdated = { ...ListCoins };
          const updateObjectCoins = listCoinsUpdated[keyNameCoin];
          if (!updateObjectCoins) return listCoinsUpdated;

          updateObjectCoins.state = updateObjectCoins.priceUsd >= ValueCoin;
          updateObjectCoins.priceUsd = ValueCoin;
          listCoinsUpdated[keyNameCoin] = updateObjectCoins;
          setResponse(listCoinsUpdated);
        });
      }
    },
    [setResponse]
  );

  return [response, upList];
};
export default useListCoins;

import { useEffect, useState } from 'react';
import useWebSocket from 'react-use-websocket';

const useSocket = (socketUrl: string): any => {
    const [response, setResponse] = useState(null);
    const {
      lastJsonMessage,
    } = useWebSocket(socketUrl);

    useEffect(() => {
      setResponse(lastJsonMessage);
    }, [lastJsonMessage])

    return response;
  };
export default useSocket;

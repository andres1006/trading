
import React, {useContext} from 'react';
import { Container} from "@material-ui/core";
//Theme & Styles
import { ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import getMuiTheme from './GetMuiTheme';

//Components
import Header from '../../components/Header';
//Context
import AppContext from '../../context/AppContext';


const Layout = ({ children }: any) => {
  const { state } = useContext(AppContext);
  const theme = getMuiTheme(state.darkMode);

  return(
    <>
      <ThemeProvider theme={theme}>
        <CssBaseline />
          <Header />
          <Container maxWidth="md" style={{'paddingTop': 30}}>
            {children}
          </Container>
      </ThemeProvider>
    </>
);
}

export default Layout;

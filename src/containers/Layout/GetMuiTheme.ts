
import { createTheme } from '@material-ui/core/styles'
import createPalette from '@material-ui/core/styles/createPalette';


const getMuiTheme = (darkTheme: boolean = false): any => {
    const type = darkTheme ? 'dark' : 'light';
    return createTheme({
        palette: createPalette({
            type: type,
            primary: {
                main: '#0E1F2B',
            },
            secondary: {
                main: '#69AFC1',
                contrastText: '#FFF',
            },
        }),
    });
};

export default getMuiTheme;
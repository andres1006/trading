import React, { useEffect, useState } from "react";
import { Grid, Fade, LinearProgress } from "@material-ui/core";
import useSocket from "../hooks/useSocket";
import axios from "axios";
//Componnets
import Graphic from "../components/Graphic";
import HeaderDetailCoin from "../components/HeaderDetailCoin";

//URL
const { REACT_APP_URI_COINCAP, REACT_APP_URI_COINCAP_SOCKET } = process.env;

const CoinDetail = () => {
  const [coin, setCoin] = useState<any>(
    JSON.parse(`${localStorage.getItem("coin")}`)
  );
  const [loading, setLoading] = useState<boolean>(true);
  const [dataSocket, setDataSocket] = useState<any[]>([]);
  const coinSocket = useSocket(
    `${REACT_APP_URI_COINCAP_SOCKET}/prices?assets=${coin.id}`
  );

  const getCoinHistory = async () => {
    const res = await axios
      .get(`${REACT_APP_URI_COINCAP}/v2/assets/bitcoin/history?interval=m1`)
      .catch(() => setLoading(false));
    if (res && res.data && res.data.data && res.data.data.length > 0) {
      const newResponse = await res.data.data.map((rowCoin: any) => {
        return { time: rowCoin.time, value: rowCoin.priceUsd };
      });
      setDataSocket(newResponse);
      setLoading(false);
      return;
    }
  };

  useEffect(() => {
    getCoinHistory();
  }, []);

  useEffect(() => {
    if (coinSocket) {
      setCoin({
        ...coin,
        state: coin.priceUsd > coinSocket[coin.id],
        priceUsd: coinSocket[coin.id],
      });
      setDataSocket((oldData: any[]): any => {
        return [
          ...oldData,
          {
            time: new Date().getTime(),
            value: coinSocket[coin.id],
          },
        ];
      });
    }
  }, [coinSocket]);

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <HeaderDetailCoin coin={coin} />
        {loading && <LinearProgress color="secondary" style={{ margin: 20 }} />}
      </Grid>
      {!loading && (
        <Fade
          in={true}
          style={{ transformOrigin: "0 0 0" }}
          {...(true ? { timeout: 1000 } : {})}
        >
          <Grid container justifyContent="center" item xs={12}>
            <Graphic data={dataSocket} />
          </Grid>
        </Fade>
      )}
    </Grid>
  );
};

export default CoinDetail;

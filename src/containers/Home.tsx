import React, { useEffect, useState, useMemo } from "react";
import {
  Container,
  Grid,
  LinearProgress,
  Button,
  Typography,
} from "@material-ui/core";
import axios from "axios";

//Components
import ListCoins from "../components/ListCoins";
import Filter from "../components/Filter";

//Hooks
import useSocket from "../hooks/useSocket";
import useListCoins from "../hooks/useListCoins";

//URL
const { REACT_APP_URI_COINCAP, REACT_APP_URI_COINCAP_SOCKET } = process.env;

const Home = () => {
  const [coins, setCoins] = useState<any>({});
  const [loading, setLoading] = useState<boolean>(true);
  const [copyCoins, setCopyCoins] = useState<any>({});
  const [viewFavoritesCoin, setViewFavoritesCoin] = useState<boolean>(true);
  const [responseList, upList] = useListCoins();
  const response = useSocket(
    `${REACT_APP_URI_COINCAP_SOCKET}/prices?assets=ALL`
  );

  useEffect(() => {
    getCoins();
  }, []);

  useEffect(() => {
    if(responseList){
      setCoins(responseList);
    }
  }, [responseList]);

  useEffect(() => {
    if(response && coins){
      upList(coins, response)
    }
  }, [response]);


  const getCoins = async () => {
    setLoading(true);
    const res = await axios
      .get(`${REACT_APP_URI_COINCAP}/v2/assets`)
      .catch(() => {console.log('error')});
    setLoading(false);
    if (res && res.data && res.data.data && res.data.data.length > 0) {
      const coinsObject = await res.data.data.reduce((objCoin: any, coin: any) => {
        coin.state = "";
        objCoin[coin.id] = coin;
        return objCoin;
      }, {});
      setCoins(coinsObject);
      setCopyCoins(coinsObject);
    }
  };

  const viewFavorites = async () => {
    if (viewFavoritesCoin) {
      setViewFavoritesCoin(false);
      if(!localStorage.getItem("coinSuscription")) return setCoins({})
      setCoins(JSON.parse(`${localStorage.getItem("coinSuscription")}`));
    } else {
      setViewFavoritesCoin(true);
      setCoins(copyCoins);
    }
  };

  const handleFilter = async (e: any) => {
    const valueFilter = e.target.value;
    if (valueFilter) {
      const coinsObject = await Object.entries(copyCoins).reduce((objCoin: any, key: any) => {
        if (key[0].includes(valueFilter.toLowerCase())) objCoin[key[0]] = copyCoins[key[0]];
        return objCoin;
      }, {});
      setCoins(coinsObject);
      return;
    }
    setCoins(copyCoins);
  };

  return useMemo(
    () => (
      <Container maxWidth="md">
        <Grid
          container
          direction="row"
          justifyContent="flex-end"
          alignItems="center"
        >
          <Filter handleFilter={handleFilter}/>
          <Button
            variant="contained"
            color="primary"
            size="small"
            onClick={viewFavorites}
          >
            {viewFavoritesCoin ? "ver Favoritos" : "Ver todos"}
          </Button>
        </Grid>
        {loading ? (
          <LinearProgress color="secondary" style={{margin: 20}}/>
        ) : Object.values(coins).length > 0 ? (
          <ListCoins listCoins={coins} />
        ) : (
          <Typography variant="h6" color="secondary" align="center">Ups no hay coins </Typography>
        )}
      </Container>
    ),
    [ coins ]
  );
};

export default Home;

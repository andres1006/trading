import React from 'react';
import { mount } from '@cypress/react';
import App from './App';

it('renders learn react link', () => {
  mount(<App />);
  cy.visit('http://localhost:3000');
  cy.get('.MuiTypography-root').contains('Mode');
});
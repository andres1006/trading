import React, { useReducer } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

// UseContext
import AppContext from '../context/AppContext';
import {initialState} from '../context/initialState';
import reducer from '../context/reducer';

//Componeents
import Home from '../containers/Home';
import CoinDetail from '../containers/CoinDetail';
import NotFound from '../containers/NotFound';
import Layout from '../containers/Layout/index';

const App: React.FC = () => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <AppContext.Provider value={{ state, dispatch }}>
      <BrowserRouter>
              <Layout>
                <Switch>
                      <Route exact path='/' component={Home} />
                      <Route exact path='/coin-detail' component={CoinDetail} />
                      <Route component={NotFound} />
                </Switch>
              </Layout>
      </BrowserRouter>
    </AppContext.Provider>
  );
}

export default App;


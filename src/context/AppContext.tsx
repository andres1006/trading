
import React from 'react';
import { ContextProps } from './schemasContext';


const AppContext = React.createContext({} as ContextProps);

export default AppContext;
import { InitialState } from './schemasContext'

export const initialState: InitialState = {
  darkMode: false,
};
import { ActionType } from './actions'

export interface InitialState {
    darkMode: boolean;
}

export interface ContextProps {
    state: InitialState;
    dispatch: any;
}

export interface Action {
    type: ActionType;
    payload: InitialState;
}
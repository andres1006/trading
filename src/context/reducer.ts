import { ActionType } from './actions';
import { InitialState, Action } from './schemasContext';

const reducer = (state: InitialState, action: Action): InitialState => {
  switch (action.type) {
    case ActionType.HANDLE_DARK_MODE:
      return {
        ...state,
        darkMode: !state.darkMode,
      };

    default:
      return state;
  }
};

export default reducer;

